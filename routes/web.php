<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => ['auth']], function () {

    Route::resource('user/books', 'BooksController');

    Route::resource('user/authors', 'AuthorsController');
    Route::get('user/viewauthors/{id}', 'AuthorsController@bookAuthors');
    Route::get('user/authors/create/{id}', 'AuthorsController@create');
    Route::post('user/filter/{type}','BooksController@filter');
});
