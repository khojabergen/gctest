<?php

namespace App\Http\Controllers;

use App\Book;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Author;
use Illuminate\Http\Request;
use Session;

class AuthorsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $authors = Author::where('name', 'LIKE', "%$keyword%")
				->orWhere('book_id', 'LIKE', "%$keyword%")
				->paginate($perPage);
        } else {
            $authors = Author::paginate($perPage);
        }

        return view('authors.index', compact('authors'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create($id = "")
    {
        return view('authors.create',compact('id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        
        Author::create($requestData);

        Session::flash('flash_message', 'Author added!');

        return redirect('user/viewauthors/'.$requestData['book_id']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $author = Author::findOrFail($id);

        return view('authors.show', compact('author'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $author = Author::findOrFail($id);

        return view('authors.edit', compact('author'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        
        $requestData = $request->all();
        
        $author = Author::findOrFail($id);
        $author->update($requestData);

        Session::flash('flash_message', 'Author updated!');

        return redirect('user/authors');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $author = Author::find($id);

        Author::destroy($author->id);

        Session::flash('flash_message', 'Author deleted!');

        return redirect('user/viewauthors/'.$author->book->id);
    }

    public function bookAuthors($id = "")
    {


        $authors = Author::where('book_id',$id)->latest()->paginate();



        return view('authors.index', compact('authors','id'));
    }
}
