<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Book;
use Illuminate\Http\Request;
use Session;

class BooksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $books = Book::where('title', 'LIKE', "%$keyword%")
				->paginate($perPage);
        } else {
            $books = Book::paginate($perPage);
        }

        return view('books.index', compact('books'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('books.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        
        Book::create($requestData);

        Session::flash('flash_message', 'Book added!');

        return redirect('user/books');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $book = Book::findOrFail($id);

        return view('books.show', compact('book'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $book = Book::findOrFail($id);

        return view('books.edit', compact('book'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        
        $requestData = $request->all();
        
        $book = Book::findOrFail($id);
        $book->update($requestData);

        Session::flash('flash_message', 'Book updated!');

        return redirect('user/books');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Book::destroy($id);

        Session::flash('flash_message', 'Book deleted!');

        return redirect('user/books');
    }

    public function filter($type = "name", Request $request){

        if($type == "count"){
            $this->validate($request, [
                'count' => 'required|numeric',
            ]);

            $books = Book::has('authors', '=',$request->count)->paginate();

            return view('books.index', compact('books'));
        }

        if($type == "name")
        {
            $this->validate($request, [
                'name' => 'required',
            ]);

            $name = $request->name;

            $books = Book::whereHas('authors', function ($query) use ($name){
                $query->where('name', 'like', '%'.$name.'%');
            })->paginate();

            return view('books.index', compact('books'));
        }

    }
}
