<div class="col-md-3">
    <div class="panel panel-default panel-flush">
        <div class="panel-heading">
            Filter
        </div>

        <div class="panel-body">
            <ul class="nav" role="tablist">
                <li role="presentation">
                    Enter authors count:
                    <form action="{{url('user/filter/count')}}" method="post">
                        <input type="number" name="count" required placeholder="3">
                        {{csrf_field()}}
                        <input type="submit" value="Submit">
                    </form><br>

                    Enter author name:
                    <form action="{{url('user/filter/name')}}" method="post">
                        <input type="text" name="name" required placeholder="hemingway">
                        {{csrf_field()}}
                        <input type="submit" value="Submit">
                    </form>
                    <br>
                    <a href="{{url('user/books')}}" class="btn btn-default">clear filter</a>
                </li>
            </ul>
        </div>
    </div>
</div>
